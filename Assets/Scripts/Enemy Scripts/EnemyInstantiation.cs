﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInstantiation : MonoBehaviour
{
public GameObject entBoss;
public GameObject ent;

    // Start is called before the first frame update
    void Start() //instnatiates all enemy prefabs, values are hard coded as the map doesnt change and there are not many enemies in the game
    {
        Instantiate(entBoss, new Vector3(31f,36f,0f), Quaternion.identity); //ll boss
        Instantiate(entBoss, new Vector3(-34.5f,80f,0f), Quaternion.identity); //ff boss
        Instantiate(ent, new Vector3(29.5f,48f,0f), Quaternion.identity); //ll ent
        Instantiate(ent, new Vector3(-18.5f, 80.5f,0f), Quaternion.identity); //ff ent
        //Instantiate(entBoss, new Vector3(-4f,5f,0f), Quaternion.identity); //spawn ent
    }
}
