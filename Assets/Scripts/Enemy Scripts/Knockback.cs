﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knockback : MonoBehaviour
{
    public float knockbackPower; 
    public float knockTime;
    public float damage;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("breakable") && this.gameObject.CompareTag("Player")) //if the gameobject being collided with is breakable
        {
            other.GetComponent<Pot>().Smash();
        }
        if (other.gameObject.CompareTag("enemy") || other.gameObject.CompareTag("Player")) //if it is the player on enemy being knocked back
        {
            Rigidbody2D hit = other.GetComponent<Rigidbody2D>(); //declaring the game object being knocked back
            if(hit != null) //if they have a rigid body 
            {
                Vector2 difference = (hit.transform.position - transform.position); //how far it wil be knocked
                difference = difference.normalized * knockbackPower; //normalised turns it into a vector1, the speed it will be knocked at
                hit.AddForce(difference, ForceMode2D.Impulse); //applies the knockback to the enemy
                if (other.gameObject.CompareTag("enemy") && other.isTrigger) //if other is the trigger hitbox of enemy
                {
                    hit.GetComponent<Enemy>().currentState = EnemyState.stagger;
                    other.GetComponent<Enemy>().Knock(hit, knockTime, damage);
                }
                if(other.gameObject.CompareTag("Player") && other.isTrigger) //if other is the trigger hitbox of player
                {
                    if (other.GetComponent<PlayerMovement>().currentState != PlayerState.stagger) //and the player is not already staggered
                    {
                        hit.GetComponent<PlayerMovement>().currentState = PlayerState.stagger;
                        other.GetComponent<PlayerMovement>().Knock(knockTime, damage);
                    }   
                }
            }
        }       
    }
}
