﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState //the different enemy states 
{
    idle,
    walk,
    meleeAttack,
    stagger
}

public class Enemy : MonoBehaviour
{
    public EnemyState currentState; //holds the value of the current state of that instance of the enemy
    public FloatValue maxHealth; //holds the value of the default health for that instance of the enemy
    public float health; //holds the value of the current health for that instance of the enemy
    /*public string enemyName; 
    public int baseAttack;
    public Transform homeposition;*/
    public float moveSpeed; //holds the value of the max move speed for that instance of the enemy
    public Transform target; //holds the value for the transform (of a gameobject) which that instance of the enemy is targeting
    public Animator animator; //allows you to reference the animator for that instance of the enemy
    public SpriteRenderer spriteRenderer; //allows you to reference the sprite renderer for that instance of the enemy
    public AudioClip deathSound; //holds the value of the death sound for that instance of the enemy
    public AudioClip hurtSound; //holds the value of the hurt sound for that instance of the enemy
    public Rigidbody2D myRigidBody;  //allows you to reference the rigidbody2d for that instance of the enemy

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake() //awake is used as it runs before the game starts and reduces the chance of errors where values are assigned incorrectly
    {
        health = maxHealth.initialValue; //makes the enemies current health full

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void TakeDamage(float damage)
    {
        health -= damage; //takes the damage away from the health pool
        if(health<=0) //if the damage taken has caused the enemy to die
        {
            StartCoroutine(EnemyDeathCo());
        }
        else
        {
            GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().enemyHurtSound(hurtSound); //plays the hurt sound using soundmanager
        }
    }

    public void Knock(Rigidbody2D myRidigbody, float knockTime, float damage)
    {
        StartCoroutine(KnockCo(myRidigbody, knockTime));
        TakeDamage(damage);      
    }

    private IEnumerator KnockCo(Rigidbody2D myRigidbody, float knockTime)
    {
        if (myRigidbody != null) //checks there is a valid rigidbody to knock
        {
            yield return new WaitForSeconds(knockTime); //knocks it back for the chosen knockback time
            myRigidbody.velocity = Vector2.zero; //stops the knockback "sliding"
            currentState = EnemyState.idle; //returns to idle state where enemy is able to function normally
            myRigidbody.velocity = Vector2.zero; //ensures there is no sliding after the state has been changed

        }
    }

    public void ChangeState(EnemyState newState)
    {
        if (currentState != newState) //updates the state of the enemy, if the new state is different
        {
            currentState = newState;
        }
    }

    public IEnumerator EnemyDeathCo()
    {
        GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().playDeathSound(deathSound); //plays the death sound
        moveSpeed = 0.5f; //slows down the speed of the enemy, this is a kind of 'death animation'
        this.GetComponent<Knockback>().damage = 0; //this enemy cannot cause any damage while in the death animation
        yield return new WaitForSeconds(1f); //waits for 1sec for sound / animation to complete
        this.gameObject.SetActive(false); //remove the enemy from the game
    }

        public void ChangeAnim(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) //checks to see if the enemy is mainly heading up/down or left/right (left/right)
        {
            if (direction.x > 0) //if heading right
            {

                SetAnimFloat(Vector2.right); //passes through (1,0) which will trigger the right moving blend tree section
                spriteRenderer.flipX = false; //if it was previously moving left, it will flip the sprite back, otherwise will do nothing

            }
            else if (direction.x < 0) //if heading left
            {
                SetAnimFloat(Vector2.left); //passes through (-1,0) which will trigger the left moving blend tree section
                spriteRenderer.flipX = true; //flips the animator to make it look like it is walking to the left even when the animation is actually it walking to the right

            }
        }
        else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y)) //checks to see if the enemy is mainly heading up/down or left/right (up/down)
        {
            if (direction.y > 0) //if heading up
            {
                SetAnimFloat(Vector2.up); //passes through (0,1) which will trigger the up moving blend tree section
                spriteRenderer.flipX = false; //if it was previously moving left, it will flip the sprite back, otherwise will do nothing

            }
            else if (direction.y < 0)
            {
                SetAnimFloat(Vector2.down); //passes through (0,-1) which will trigger the downwards moving blend tree section
                spriteRenderer.flipX = false; //if it was previously moving left, it will flip the sprite back, otherwise will do nothing

            }
        }
    }
     public void SetAnimFloat(Vector2 setVector) //sets the move X and Y floats of the animator equal to the vector passed in
    {
        animator.SetFloat("moveX", setVector.x);
        animator.SetFloat("moveY", setVector.y);
    }
}
