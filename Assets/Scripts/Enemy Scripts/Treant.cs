﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treant : EnemyMelee //inherits from enemymelee, enemy
{
    // Start is called before the first frame update
    void Start()
    {
        currentState = EnemyState.idle;
        target = GameObject.FindWithTag("Player").transform;
        myRigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckDistance();
    }

    private new void CheckDistance()
    {
        if (Vector3.Distance(target.position, transform.position) <= chaseRadius
            && Vector3.Distance(target.position, transform.position) >= attackRadius)
        {
            animator.SetBool("moving", true);
            if ((currentState == EnemyState.idle || currentState == EnemyState.walk)
                && currentState != EnemyState.stagger)
            {
                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, (moveSpeed * Time.deltaTime));

                myRigidBody.MovePosition(temp);
                ChangeAnim(temp - transform.position);
                ChangeState(EnemyState.walk);
                
            }

        }
        else if (Vector3.Distance(target.position, transform.position) > chaseRadius)
        {
            animator.SetBool("moving", false); //stops the enemy moving which will return the enemy to the idle animation

        }
    }
}
