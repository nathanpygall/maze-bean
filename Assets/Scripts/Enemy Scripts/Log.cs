﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log : EnemyMelee
{

    // Start is called before the first frame update
    void Start()
    {
        currentState = EnemyState.idle;
        target = GameObject.FindWithTag("Player").transform;
        myRigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void FixedUpdate() //goes on physics ticks rather than game frames
    {
        CheckDistance();
    }

    private new void CheckDistance()
    {
        if (Vector3.Distance(target.position, transform.position) <= chaseRadius
            && Vector3.Distance(target.position, transform.position) >= attackRadius)
        {
            if ((currentState == EnemyState.idle || currentState == EnemyState.walk)
                && currentState != EnemyState.stagger)
            {
                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, (moveSpeed * Time.deltaTime));

                myRigidBody.MovePosition(temp);
                ChangeAnim(temp - transform.position);
                ChangeState(EnemyState.walk);
                animator.SetBool("wakeUp", true); //the log will only function when wakeUp is true
            }

        }
        else if(Vector3.Distance(target.position, transform.position) > chaseRadius)
        {
            animator.SetBool("wakeUp", false); //whenever the player is not in range, it will go to sleep and the log goes to sleep when wakeUp is false
        }
    }
}
