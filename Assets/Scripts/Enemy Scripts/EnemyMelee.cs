﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMelee : Enemy //inherits from enemy
{
    public float chaseRadius; //a radius around the enemy in which the player will be chased
    public float attackRadius; //a radius around the enemy in which the player can be attacked from
    
    void FixedUpdate()
    {
        CheckDistance();
    }
    public void CheckDistance()
    {
        if (Vector3.Distance(target.position, transform.position) <= chaseRadius //if the distance between this enemy and the player is less than the chase distance 
            && Vector3.Distance(target.position, transform.position) >= attackRadius) //and the distance between this enemy and the player is more than the attack radius
        {
            if ((currentState == EnemyState.idle || currentState == EnemyState.walk)  
                && currentState != EnemyState.stagger) //checks the enemy is able to move towards the player in the state it is currently in
            {
                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, (moveSpeed * Time.deltaTime)); //stores where the enemy will move to

                myRigidBody.MovePosition(temp); //moves this enemy to the new position
                ChangeAnim(temp - transform.position); //calls changeanim and passes in the total distance moved on both axis
                ChangeState(EnemyState.walk); 

            }

        }
        else if(Vector3.Distance(target.position, transform.position) > chaseRadius)
        {
            //each melee enemy acts differently when the player is not in distance of the chase radius so this is left blank to avoid errors
        }
    }



}
