﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : Interactable
{
    public BoolValue beenShown;


    void Start()
    {
        beenShown.currentValue = false;
    }
    void Update()
    {
        if (playerInRange && beenShown.currentValue == false)
        {
            if(dialogbox.activeInHierarchy)
            {
                dialogbox.SetActive(false);
            }
            else
            {
                dialogbox.SetActive(true);
                dialogText.text = dialog;
                beenShown.currentValue = true;
            }
           
        }
        if (Input.GetKeyDown(KeyCode.E) && beenShown.currentValue == true && playerInRange)
        {
            dialogbox.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !other.isTrigger)
        {
            playerInRange = true;
        }    
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            dialogbox.SetActive(false);
            playerInRange = false;
        }
    }
}
