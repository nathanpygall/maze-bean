﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeDoor : SceneTransition
{
    public BoolValue hasFirstKey;
    public BoolValue hasSecondKey;
    public string noKeysDialog;
    public string oneKeyDialog;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public new void Update()
    {
        Debug.Log(hasFirstKey.currentValue);
        Debug.Log(hasSecondKey.currentValue);
        if (Input.GetKeyDown(KeyCode.E) && playerInRange && hasFirstKey.currentValue && hasSecondKey.currentValue)
        {
            playerStorage.initialValue = playerPosition;
            StartCoroutine(FadeCo());
            //SceneManager.LoadScene(sceneToLoad);
        }
        else if (Input.GetKeyDown(KeyCode.E) && playerInRange && !(hasFirstKey.currentValue || hasSecondKey.currentValue))
        {
            if(dialogbox.activeInHierarchy)
            {
                dialogbox.SetActive(false);
            }
            else
            {
                dialogbox.SetActive(true);
                dialogText.text = noKeysDialog;
            }
        }
        else if (Input.GetKeyDown(KeyCode.E) && playerInRange && (!hasFirstKey.currentValue || !hasSecondKey.currentValue))
        {
            if(dialogbox.activeInHierarchy)
            {
                dialogbox.SetActive(false);
            }
            else
            {
                dialogbox.SetActive(true);
                dialogText.text = oneKeyDialog;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            context.Raise();
            playerInRange = false;
            dialogbox.SetActive(false);
        }   
    }
}
