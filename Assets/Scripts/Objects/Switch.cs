﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : Interactable
{
    public SpriteRenderer spriteRenderer;
    private bool hasBeenFlipped;
    public Sprite flippedSwitchSprite;
    public BoolValue keyChecker;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        hasBeenFlipped = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && playerInRange && !hasBeenFlipped)
        {
            spriteRenderer.sprite = flippedSwitchSprite;
            GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().switchSound();  
            keyChecker.currentValue = true;
            hasBeenFlipped = true;
        }
        else if (Input.GetKeyDown(KeyCode.E) && playerInRange && hasBeenFlipped)
        {
            Debug.Log(dialogbox.activeInHierarchy);
            dialogbox.SetActive(true);
            dialogText.text = dialog;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            context.Raise();
            playerInRange = false;
            dialogbox.SetActive(false);
        }
    }

     private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !other.isTrigger)
        {
            context.Raise();
            playerInRange = true;
            dialogbox.SetActive(false);
        }    
    }
}
