﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Interactable : MonoBehaviour
{

    public bool playerInRange; //no interactable will work unless this bool is true
    public GameObject dialogbox;
    public Text dialogText;
    public string dialog;
    public SignalObject context;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !other.isTrigger) //if the hitbox of the player enters the interactable range
        {
            context.Raise(); //will trigger the signal object (toggle)
            playerInRange = true;
        }    
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger) // if the player has left the interactable range
        {
            context.Raise(); //will trigger the signal object (toggle)
            playerInRange = false;
        }
    }
}
