﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomMove : MonoBehaviour
{

    public Vector2 cameraChangeMin;
    public Vector2 cameraChangeMax;
    public Vector3 playerChange;
    private CameraMovement cam;
    public bool needText;
    public string areaName;
    public GameObject text;
    public Text areaText;
    public int enteringRoomNumber;



    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<CameraMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            cam.minPosition.x += cameraChangeMin.x;
            cam.minPosition.y += cameraChangeMin.y;

            cam.maxPosition.x += cameraChangeMax.x;
            cam.maxPosition.y += cameraChangeMax.y;

            other.transform.position += playerChange;

            if (needText)
            {
                StartCoroutine(areaNameCo());
            }

            GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().playTransition(enteringRoomNumber);  

        }
    }

    private IEnumerator areaNameCo()
    {
        PlayerMovement.isInputEnabled = false;
        GameObject.Find("Player").GetComponent<PlayerMovement>().StopMoving();

        text.SetActive(true);
        areaText.text = areaName;
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 100; i++)
        {
            areaText.color = new Color(
                areaText.color.r,
                areaText.color.g,
                areaText.color.b,
                areaText.color.a - 0.01f);
            yield return new WaitForEndOfFrame();
        }
        areaText.color = new Color(
                areaText.color.r,
                areaText.color.g,
                areaText.color.b,
                areaText.color.a + 1f);
        text.SetActive(false);
        PlayerMovement.isInputEnabled = true;

    }

}
