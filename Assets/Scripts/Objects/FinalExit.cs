﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalExit : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) //visual prompt on screen when the game has been completed to press Q
        {
            SceneManager.LoadScene("StartMenu"); //returns the user to the start menu
        }
    }
}
