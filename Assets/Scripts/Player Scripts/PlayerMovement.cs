﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum PlayerState
{
    walk,
    meleeAttack,
    rangedAttack,
    interact,
    stagger,
    idle
}
public class PlayerMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody2D myRigidBody;
    private Vector3 change;
    public Animator animator;
    private SpriteRenderer spriteRenderer;
    public static bool isInputEnabled = true;
    public PlayerState currentState;
    public GameObject hitbox;
    public FloatValue currentHealth;
    public SignalObject playerHealthSignalObject;
    public VectorValue startingPosition;

    void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        currentState = PlayerState.walk;
        animator = GetComponent<Animator>();
        myRigidBody = GetComponent<Rigidbody2D>();
        animator.SetFloat("moveX", 0);
        animator.SetFloat("moveY", -1);
        spriteRenderer = GetComponent<SpriteRenderer>();
        transform.position = startingPosition.initialValue;
    
    }

    // Update is called once per frame
    void Update()
    {
        if (change.x < 0)
        {
            hitbox.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 270f));
        }
        else if (change.x > 0)
        {
            hitbox.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90f));

        }
        if (change.y > 0)
        {
            hitbox.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 180f));
        }
        else if (change.y < 0)
        {
            hitbox.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0f));
        }


        if (isInputEnabled)
        {
            change = Vector3.zero;
            change.x = Input.GetAxisRaw("Horizontal");
            change.y = Input.GetAxisRaw("Vertical");
            //Debug.Log(change);
            if (Input.GetButtonDown("attack") && currentState != PlayerState.meleeAttack
                && currentState != PlayerState.stagger)
            {
                StartCoroutine(AttackCo());
            }
            else if (currentState == PlayerState.walk || currentState == PlayerState.idle)
            {
                UpdateAnimationAndMove();
            }

        }
    }

    private IEnumerator AttackCo()
    {

        animator.SetBool("attacking", true);
        currentState = PlayerState.meleeAttack;
        yield return null;
        animator.SetBool("attacking", false);
        yield return new WaitForSeconds(.22f);
        currentState = PlayerState.walk;
    }

    void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharachter();
            animator.SetFloat("moveX", change.x);
            animator.SetFloat("moveY", change.y);
            animator.SetBool("moving", true);

            if (change.x < 0)
            {
                spriteRenderer.flipX = true;
            }
            if (change.x > 0)
            {
                spriteRenderer.flipX = false;
            }
        }
        else
        {
            StopMoving();
        }
    }

    void MoveCharachter()
    {
        change.Normalize();
        myRigidBody.MovePosition(
            transform.position + change * speed * Time.deltaTime
        );
    }

    public void Knock(float knockTime, float damage)
    {
        currentHealth.runtimeValue -= damage;
        if (currentHealth.runtimeValue > 0)
        {
            playerHealthSignalObject.Raise();
            StartCoroutine(KnockCo(knockTime));
            PlayHurtSound();
        }
        else
        {
            StartCoroutine(PlayerDeathCo());
        }
    }

    private IEnumerator KnockCo(float knockTime)
    {
        if (myRigidBody != null)
        {
            //Debug.Log("knockco running");
            //Debug.Log(knockTime);
            yield return new WaitForSeconds(knockTime); //knocks it back for the chosen knockback time
            //Debug.Log("does this run");
            //Debug.Log("pre ve" + myRigidBody.velocity);
            myRigidBody.velocity = Vector2.zero; //stops the knockback "sliding"
            //Debug.Log("post ve" + myRigidBody.velocity);
            currentState = PlayerState.walk;
            myRigidBody.velocity = Vector2.zero;

        }
    }
private void PlayHurtSound()
{
    GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().playerHurtSound();
}
    public void StopMoving()
    {
        animator.SetBool("moving", false);
    }

#region playerdeath
    public GameObject deathText;
    public AudioClip deathSound;

    private IEnumerator PlayerDeathCo()
    {
        deathText.SetActive(true);
        GameObject.Find("HeartContainer").GetComponent<HeartManager>().Death();
        GameObject.Find("SoundManagerObj").GetComponent<SoundManager>().playDeathSound(deathSound);
        spriteRenderer.enabled = false;
        StopMoving();
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(5f);
        Time.timeScale = 1f;
        Debug.Log("pre init");
        currentHealth.runtimeValue = currentHealth.initialValue;
        Debug.Log("post init");
        SceneManager.LoadScene("SampleScene");
    }
   
#endregion   
}
