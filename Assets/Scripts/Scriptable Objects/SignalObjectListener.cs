﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SignalObjectListener : MonoBehaviour
{
public SignalObject signalObject;
public UnityEvent signalEvent;

    public void OnSignalRaised()
    {
        signalEvent.Invoke();
    }

    private void OnEnable() 
    {
        signalObject.RegisterListener(this);
    }
    private void OnDisable() 
    {
        signalObject.DeRegisterListener(this);
    }
}
