﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BoolValue : ScriptableObject, ISerializationCallbackReceiver
{
    public bool currentValue;
    public bool defaultValue;
    public void OnAfterDeserialize(){ currentValue = defaultValue;}
    public void OnBeforeSerialize(){}
}
