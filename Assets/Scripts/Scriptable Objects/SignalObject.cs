﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SignalObject : ScriptableObject
{
    public  List<SignalObjectListener> listeners = new List<SignalObjectListener>();

    public void Raise() //going thru backwards incase somethin is removed to prevent out of range exception
    {
        for (int i = listeners.Count - 1; i>= 0; i --)
        {
            listeners[i].OnSignalRaised();
        }
    }

    public void RegisterListener(SignalObjectListener listener)
    {
        listeners.Add(listener);
    }

    public void DeRegisterListener(SignalObjectListener listener)
    {
        listeners.Remove(listener);
    }
}
