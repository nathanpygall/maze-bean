﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu] //allows to create as an object using right click
public class FloatValue : ScriptableObject, ISerializationCallbackReceiver //this script cannot be attached to anything in the scene
{
    public float initialValue;
    [HideInInspector]
    public float runtimeValue;

    public void OnAfterDeserialize()
    {
        runtimeValue = initialValue;
    }  
    public void OnBeforeSerialize()
    {

    }
}
