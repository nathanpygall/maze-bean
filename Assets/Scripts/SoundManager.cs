﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class SoundManager : MonoBehaviour
{
    public AudioClip[] generalSoundEffects;
    public AudioClip[] generalMusic;  
    public AudioClip[] ambientSoundEffects;
    public AudioClip[] transitionSoundEffects;
    static AudioSource audioSrc;
    public AudioMixer mixer;
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();        
    }
    public void playAmbient()
    {
        int soundNo = Random.Range(0,ambientSoundEffects.Length);        
        audioSrc.PlayOneShot(ambientSoundEffects[soundNo]);
    }
    public void playTransition(int roomNo)
    {
        audioSrc.PlayOneShot(transitionSoundEffects[roomNo]);
    }
    public void playDeathSound(AudioClip deathSound)
    {
        audioSrc.PlayOneShot(deathSound);
    }
    public void enemyHurtSound(AudioClip hurtSound)
    {
        audioSrc.PlayOneShot(hurtSound);
    }
    public void playerHurtSound()
    {
        int soundNo = Random.Range(1, 4);
        audioSrc.PlayOneShot(generalSoundEffects[soundNo]);
    }
    public void switchSound()
    {
        audioSrc.PlayOneShot(generalSoundEffects[5]);
    }
    public void potBreakSound()
    {
        audioSrc.PlayOneShot(generalSoundEffects[6]);
    }
}
